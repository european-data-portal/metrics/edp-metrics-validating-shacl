package io.piveau.validating

import io.piveau.vocabularies.readTurtleResource
import io.piveau.vocabularies.readXmlResource
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFDataMgr
import java.io.InputStream

object Vocabularies {

    const val DEFAULT_SHAPE_MODEL = "dcatap201"

    val vocabularies = ModelFactory.createDefaultModel()!!

    val dcatap201: Model by lazy {
        ModelFactory.createDefaultModel().apply {
            readTurtleResource("rdf/shapes/2.0.1/dcat-ap_2.0.1_shacl_shapes.ttl")
            readTurtleResource("rdf/shapes/2.0.1/dcat-ap_2.0.1_shacl_deprecateduris.ttl")
            readTurtleResource("rdf/shapes/2.0.1/dcat-ap_2.0.1_shacl_mdr-vocabularies.shape.ttl")
        }
    }

    val dcatap200: Model by lazy {
        ModelFactory.createDefaultModel().apply {
            readTurtleResource("rdf/shapes/2.0.0/dcat-ap_2.0.0_shacl_shapes.ttl")
            readTurtleResource("rdf/shapes/2.0.0/dcat-ap_2.0.0_shacl_deprecateduris.ttl")
            readTurtleResource("rdf/shapes/2.0.0/dcat-ap_2.0.0_shacl_mdr-vocabularies.shape_.ttl")
        }
    }

    val dcatap121: Model by lazy {
        ModelFactory.createDefaultModel().apply {
            readTurtleResource("rdf/shapes/1.2.1-hotfix/dcat-ap_1.2.1_shacl_shapes.ttl")
            readTurtleResource("rdf/shapes/1.2.1-hotfix/dcat-ap_1.2.1_shacl_mandatory-classes.shapes.ttl")
            readTurtleResource("rdf/shapes/1.2.1-hotfix/dcat-ap_1.2.1_shacl_mdr-vocabularies.shape_.ttl")
        }
    }

    val dcatap121orig: Model by lazy {
        ModelFactory.createDefaultModel().apply {
            readTurtleResource("rdf/shapes/1.2.1/dcat-ap_1.2.1_shacl_shapes.ttl")
            readTurtleResource("rdf/shapes/1.2.1/dcat-ap_1.2.1_shacl_mandatory-classes.shapes.ttl")
            readTurtleResource("rdf/shapes/1.2.1/dcat-ap_1.2.1_shacl_mdr-vocabularies.shape.ttl")
        }
    }

    val dcatap12: Model by lazy {
        ModelFactory.createDefaultModel().apply {
            readTurtleResource("rdf/shapes/1.2/dcat-ap.shapes.ttl")
            readTurtleResource("rdf/shapes/1.2/dcat-ap-mandatory-classes.shapes.ttl")
            readTurtleResource("rdf/shapes/1.2/dcat-ap-mdr-vocabularies.shapes.ttl")
        }
    }

    val dcatap11: Model by lazy {
        ModelFactory.createDefaultModel().apply {
            readTurtleResource("rdf/shapes/1.1-201706/dcat-ap.shapes.ttl")
            readTurtleResource("rdf/shapes/1.1-201706/dcat-ap-mandatory-classes.shapes.ttl")
            readTurtleResource("rdf/shapes/1.1-201706/dcat-ap-mdr-vocabularies.shapes.ttl")
        }
    }

    val dcatap11orig: Model by lazy {
        ModelFactory.createDefaultModel().apply {
            readTurtleResource("rdf/shapes/1.1-201608/dcat-ap.shapes.ttl")
            readTurtleResource("rdf/shapes/1.1-201608/dcat-ap-mandatory-classes.shapes.ttl")
            readTurtleResource("rdf/shapes/1.1-201608/dcat-ap-mdr-vocabularies.shapes.ttl")
        }
    }

    init {
        with(vocabularies) {
            readXmlResource("ADMS_SKOS_v1.00.rdf")
            readXmlResource("continents-skos.rdf")
            readXmlResource("corporatebodies-skos.rdf")
            readXmlResource("countries-skos.rdf")
            readXmlResource("data-theme-skos.rdf")
            readXmlResource("filetypes-skos.rdf")
            readXmlResource("frequencies-skos.rdf")
            readXmlResource("languages-skos.rdf")
            readXmlResource("places-skos.rdf")
        }
    }

}
