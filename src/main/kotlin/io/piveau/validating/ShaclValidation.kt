package io.piveau.validating

import io.piveau.vocabularies.vocabulary.SHACL
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.RDFNode
import org.apache.jena.sparql.vocabulary.FOAF
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.RDF
import org.topbraid.shacl.validation.ValidationUtil
import java.util.*

fun validateModel(model: Model, shapeModel: String = Vocabularies.DEFAULT_SHAPE_MODEL): Model {

    model.add(Vocabularies.vocabularies)

    val shapes = when(shapeModel) {
        "dcatap201" -> Vocabularies.dcatap201
        "dcatap200" -> Vocabularies.dcatap200
        "dcatap121" -> Vocabularies.dcatap121
        "dcatap121orig" -> Vocabularies.dcatap121orig
        "dcatap12" -> Vocabularies.dcatap12
        "dcatap11" -> Vocabularies.dcatap11
        "dcatap11orig" -> Vocabularies.dcatap11orig
        else -> Vocabularies.dcatap201
    }

    val engine = ValidationUtil.createValidationEngine(model, shapes, false)
    engine.applyEntailments()

    val nodes = ArrayList<RDFNode>()
    with(nodes) {
        addAll(model.listResourcesWithProperty(RDF.type, DCAT.Catalog).toList())
        addAll(model.listResourcesWithProperty(RDF.type, DCAT.CatalogRecord).toList())
        addAll(model.listResourcesWithProperty(RDF.type, DCAT.Dataset).toList())
        addAll(model.listResourcesWithProperty(RDF.type, DCAT.Distribution).toList())
        addAll(model.listResourcesWithProperty(RDF.type, FOAF.Agent).toList())
    }
    nodes.forEach {
        engine.validateNode(it.asNode())
    }
    engine.updateConforms()
    val report = engine.report
    report.model.setNsPrefix("sh", SHACL.NS)

    return report.model
}
