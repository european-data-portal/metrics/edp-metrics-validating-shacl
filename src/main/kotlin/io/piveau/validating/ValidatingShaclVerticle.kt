package io.piveau.validating

import io.piveau.dqv.addAnnotation
import io.piveau.dqv.createMetricsGraph
import io.piveau.dqv.listMetricsModels
import io.piveau.dqv.replaceAnnotation
import io.piveau.pipe.PipeContext
import io.piveau.rdf.*
import io.piveau.vocabularies.vocabulary.*
import io.vertx.core.AbstractVerticle
import io.vertx.core.Promise
import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonObject
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Resource
import org.apache.jena.riot.Lang
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.OA
import org.apache.jena.vocabulary.RDF
import org.slf4j.LoggerFactory
import java.nio.charset.StandardCharsets
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue

class ValidatingShaclVerticle : AbstractVerticle() {

    private val log = LoggerFactory.getLogger(this.javaClass)

    @ExperimentalTime
    override fun start(startPromise: Promise<Void>) {
        vertx.eventBus().consumer(ADDRESS_PIPE, this::handlePipe)
        vertx.eventBus().consumer(ADDRESS_REPORT, this::handleReport)

        startPromise.complete()
    }

    companion object {
        const val ADDRESS_PIPE: String = "io.piveau.pipe.validating.shacl.queue"
        const val ADDRESS_REPORT: String = "io.piveau.report.validating.shacl.queue"
    }

    @ExperimentalTime
    private fun handlePipe(message: Message<PipeContext>) {
        val pipeContext = message.body()

        if (pipeContext.config.getBoolean("skip", false)) {
            pipeContext.log().debug("Data validation skipped: {}", pipeContext.dataInfo)
            pipeContext.pass()
            return
        }

        val content =
            if (pipeContext.pipeManager.isBase64Payload) pipeContext.binaryData else pipeContext.stringData.toByteArray()

        val dataset = content.toDataset(pipeContext.mimeType?.asRdfLang() ?: Lang.TRIG)
        val model = ModelFactory.createDefaultModel().add(dataset.defaultModel)

        val shapeModel = pipeContext.config.getString("shapeModel", Vocabularies.DEFAULT_SHAPE_MODEL)
        val measured = measureTimedValue {
            validateModel(model, shapeModel)
        }
        pipeContext.log().info("Dataset validated ({}): {}", measured.duration.inMilliseconds, pipeContext.dataInfo)
        val report = measured.value

        val resource = model.listSubjectsWithProperty(RDF.type, model.resourceType()).next()

        if (dataset.listMetricsModels().isEmpty()) {
            val metricsModel = dataset.createMetricsGraph("urn:${pipeContext.pipe.header.context?.asNormalized()}:${pipeContext.pipe.header.name}")
            metricsModel.add(report)
            metricsModel.addAnnotation(resource, OA.describing, report.listSubjectsWithProperty(RDF.type, SHACL.ValidationReport).next())
        } else {
            val metricsModel = dataset.listMetricsModels()[0]
            metricsModel.add(report)
            metricsModel.replaceAnnotation(resource, OA.describing, report.listSubjectsWithProperty(RDF.type, SHACL.ValidationReport).next())
        }
        pipeContext.log().debug("Dataset content: {}", dataset.asString())
        pipeContext.setResult(dataset.asString(), RDFMimeTypes.TRIG, pipeContext.dataInfo).forward()
        report.close()
        model.close()
        dataset.close()
    }

    @ExperimentalTime
    private fun handleReport(message: Message<JsonObject>) {
        val contentType = message.body().getString("contentType").asRdfLang()
        val content = message.body().getString("content")
        val acceptableContentType = message.body().getString("acceptableContentType")
        val shapeModel = message.body().getString("shapeModel")
        val model = content.toByteArray(StandardCharsets.UTF_8).toModel(contentType)
        val measured = measureTimedValue {
            validateModel(model, shapeModel)
        }
        log.debug("RDF validated in {} milliseconds", measured.duration.inMilliseconds)
        val report = measured.value
        message.reply(report.asString(acceptableContentType.asRdfLang()))
    }

}

private fun Model.resourceType(): Resource = when {
    contains(null, RDF.type, DCAT.Catalog) -> DCAT.Catalog
    contains(null, RDF.type, DCAT.Dataset) -> DCAT.Dataset
    contains(null, RDF.type, DCAT.Distribution) -> DCAT.Distribution
    contains(null, RDF.type, DCAT.CatalogRecord) -> DCAT.CatalogRecord
    // fallback
    else -> DCAT.Dataset
}
