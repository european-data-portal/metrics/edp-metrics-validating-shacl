package io.piveau

import io.piveau.pipe.connector.PipeConnector
import io.piveau.validating.ValidatingShaclVerticle
import io.piveau.validating.Vocabularies
import io.vertx.config.ConfigRetriever
import io.vertx.config.ConfigRetrieverOptions
import io.vertx.config.ConfigStoreOptions
import io.vertx.core.DeploymentOptions
import io.vertx.core.Launcher
import io.vertx.core.http.HttpMethod
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory
import io.vertx.ext.web.handler.CorsHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.kotlin.config.getConfigAwait
import io.vertx.kotlin.core.json.array
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.awaitBlocking
import io.vertx.kotlin.coroutines.awaitResult

class MainVerticle : CoroutineVerticle() {

    override suspend fun start() {
        val options = ConfigStoreOptions().apply {
            type = "env"
            config = json { obj("keys" to array("PIVEAU_SHACL_VERTICLE_INSTANCES", "PIVEAU_SHACL_WORKER_POOL_SIZE") ) }
        }
        val config = ConfigRetriever.create(vertx, ConfigRetrieverOptions().addStore(options)).getConfigAwait()

        val instances = config.getInteger("PIVEAU_SHACL_VERTICLE_INSTANCES", DeploymentOptions.DEFAULT_INSTANCES)
        val poolSize = config.getInteger("PIVEAU_SHACL_WORKER_POOL_SIZE", 20)
        awaitBlocking { Vocabularies.vocabularies } // trigger loading
        awaitResult<String> {
            vertx.deployVerticle(
                ValidatingShaclVerticle::class.java,
                DeploymentOptions().setInstances(instances).setWorkerPoolSize(poolSize).setWorker(true),
                it
            )
        }
        awaitResult<PipeConnector> { PipeConnector.create(vertx, DeploymentOptions(), it) }.apply {
            consumer(ValidatingShaclVerticle.ADDRESS_PIPE)
            OpenAPI3RouterFactory.create(vertx, "webroot/shacl/openapi.yaml") {
                if (it.succeeded()) {
                    it.result().addHandlerByOperationId("validationReport", this@MainVerticle::handleValidation)
                    val router = it.result().router
                    router.route().order(0).handler(CorsHandler.create("*").allowedMethods(setOf(HttpMethod.POST)))
                    router.route("/*").handler(StaticHandler.create())
                    subRouter("/shacl", it.result().router)
                } else {
                    throw it.cause()
                }
            }
        }
    }

    private fun handleValidation(context: RoutingContext) {
        val shapeModel = context.queryParam("shapeModel").firstOrNull() ?: Vocabularies.DEFAULT_SHAPE_MODEL
        val message = JsonObject()
            .put("contentType", context.request().getHeader("Content-Type"))
            .put("content", context.bodyAsString)
            .put("acceptableContentType", context.acceptableContentType ?: "text/turtle")
            .put("shapeModel", shapeModel)

        vertx.eventBus().request<String>(ValidatingShaclVerticle.ADDRESS_REPORT, message) {
            if (it.succeeded()) {
                context.response().putHeader("Content-Type", message.getString("acceptableContentType"))
                    .end(it.result().body())
            } else {
                context.response().setStatusCode(500).end()
            }
        }
    }

}

fun main(args: Array<String>) {
    Launcher.executeCommand("run", *(args.plus(MainVerticle::class.java.name)))
}
