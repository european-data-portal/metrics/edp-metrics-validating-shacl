package io.piveau.validating

import io.piveau.vocabularies.readTurtleResource
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.apache.jena.rdf.model.ModelFactory
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.system.measureTimeMillis

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ValidatingShaclTest {

    @Test
    fun `Validate a dataset 5 times and measure the durations`() = runBlocking<Unit> {
        ModelFactory.createDefaultModel().apply {
            readTurtleResource("test.ttl")
            repeat(10) {
                launch {
                    val milli = measureTimeMillis {
                        val report = validateModel(this@apply)
                    }
                    println("Validation took $milli milliseconds")
                }
            }
        }
    }

}