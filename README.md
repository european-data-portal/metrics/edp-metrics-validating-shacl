# Validating SHACL
Microservice for validating data in a pipe.

The service is based on the [pipe-connector](https://gitlab.com/european-data-portal/pipe/edp-pipe-connector) library. Any configuration applicable for the pipe-connector can also be used for this service.

## Table of Contents
1. [Build](#build)
1. [Run](#run)
1. [Docker](#docker)
1. [Configuration](#configuration)
    1. [Pipe](#pipe)
    1. [Environment](#environment)
    1. [Logging](#logging)

## Build

Requirements:
 * Git
 * Maven 3
 * Java 11

```bash
$ git clone ...
$ mvn package
```
 
## Run

```bash
$ java -jar target/validating-shacl.jar
```

## Docker

Build docker image:
```bash
$ docker build -t edp/validating-shacl .
```

Run docker image:
```bash
$ docker run -it -p 8080:8080 edp/validating-shacl
```

## Configuration

### Pipe
* `skip`: Skip validation
* `shapeModel`: The DCAT-AP shacl shape version to use. Valid values are
   * `shapes200`
   * `shapes121`
   * `shapes121orig`
   * `shapes12`
   * `shapes11`
   * `shapes11orig`
     
### Environment
See also [pipe-connector](https://gitlab.com/european-data-portal/pipe/edp-pipe-connector)

| Variable| Description | Default Value |
| :--- | :--- | :--- |
| `PIVEAU_SHACL_VERTICLE_INSTANCES` | Configure the number of verticle instances. This should be maximum the number of cores | `1` |
| `PIVEAU_SHACL_WORKER_POOL_SIZE` | Configure the worker pool size for the shacl verticles. It seems to be less memory consuming when equally set to the number of instances | `20` |

### Logging
See [logback](https://logback.qos.ch/documentation.html) documentation for more details

| Variable| Description | Default Value |
| :--- | :--- | :--- |
| `PIVEAU_PIPE_LOG_APPENDER` | Configures the log appender for the pipe context | `STDOUT` |
| `PIVEAU_LOGSTASH_HOST`            | The host of the logstash service | `logstash` |
| `PIVEAU_LOGSTASH_PORT`            | The port the logstash service is running | `5044` |
| `PIVEAU_PIPE_LOG_PATH`     | Path to the file for the file appender | `logs/piveau-pipe.%d{yyyy-MM-dd}.log` |
| `PIVEAU_PIPE_LOG_LEVEL`    | The log level for the pipe context | `INFO` |
| `PIVEAU_LOG_LEVEL`    | The general log level for the `io.piveau` package | `INFO` |

