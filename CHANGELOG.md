# ChangeLog

## Unreleased

**Added:**
* DCAT-AP Shacl Shapes 2.0.1

**Changed:**
* Default Shacl Shapes set to version 2.0.1

## [1.1.3](https://gitlab.fokus.fraunhofer.de/viaduct/piveau-transforming-js/tags/1.1.3) (2020-04-03)

**Fixed:**
* Include report in annotation

**Changed:**
* Vert.x and dependencies update

## [1.1.2](https://gitlab.fokus.fraunhofer.de/viaduct/piveau-transforming-js/tags/1.1.2) (2020-03-11)

**Changed:**
* Explicitly closing models and datasets after use

## [1.1.1](https://gitlab.fokus.fraunhofer.de/viaduct/piveau-transforming-js/tags/1.1.1) (2020-03-08)

**Changed:**
* Use utils metrics API instead of own implementation

## [1.1.0](https://gitlab.fokus.fraunhofer.de/viaduct/piveau-transforming-js/tags/1.1.0) (2020-03-06)

**Added:**
* Configuration of verticle instances and worker pool size
 
## [1.0.3](https://gitlab.fokus.fraunhofer.de/viaduct/piveau-transforming-js/tags/1.0.3) (2020-03-03)

**Fixed:**
* Pass payload correctly when skipped

## [1.0.2](https://gitlab.fokus.fraunhofer.de/viaduct/piveau-transforming-js/tags/1.0.2) (2020-03-02)

**Fixed:**
* Hot-fix for openapi yaml load in index html 

## [1.0.1](https://gitlab.fokus.fraunhofer.de/viaduct/piveau-transforming-js/tags/1.0.1) (2020-03-02)

**Fixed:**
* Hot-fix for shacl validation service index page 

## [1.0.0](https://gitlab.fokus.fraunhofer.de/viaduct/piveau-transforming-js/tags/1.0.0) (2020-02-28)

Initial production release

**Added:**
* check for existing quality metadata
* `/validation/report` for direct validation interface
* All shacl shape versions
* Configuration shape version via pipe and and REST API 
  
**Changed:**
* Updated dcat-ap shacl files
* `attached` default value to true
* Read data as dataset
* Pre-load vocabularies and shapes on start-up
* Sub-route `/validation/report` joint now at `/shacl` for compliance
 
**Fixed:**
* Validation report with conforms
* Validate all nodes in a model
